<?php

function cincoVocales ($str) {
    $myArray = str_split($str);
    $vowels = array();
    foreach($myArray as $char) {
        if ($char == "a" || $char == "e" || $char == "i" || $char == "o" || $char == "u"){
            $vowels[] = $char;
        }
}
    $result = array_unique($vowels);
    print (count($result) === 5) ? "LA PALABRA CONTIENE LAS 5 VOCALES" : "NO CONTIENE TODAS LAS VOCALES";
 
}

cincoVocales("murcielago");
?>